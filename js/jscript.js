




//Dropdownlist-toggle
$('.drop-down .d-d-c ').click(function(){
    $( this ).closest('.drop-down').toggleClass('open');
});
//Dropdownlist-select
$('.drop-down ul li').click(function(){
    //Get Values of clicked list item
    let val = $( this ).attr('value');
    let content = $( this ).html();

    //Apply Values to titel and input box
    let dropdown = $( this ).closest('.drop-down');
    dropdown.find('.d-d-c').html(content);
    dropdown.find('input[name="input-for-city"]').attr('value', val);

    //Close Select box
    $( this ).closest('.drop-down').toggleClass('open');

    //Set selected-property for styling
    $( this ).siblings('li').removeAttr('selected');
    $( this ).attr('selected', true);
});

//Dropdownlist-toggle
$('.drop-down .d-d-s ').click(function(){
    $( this ).closest('.drop-down').toggleClass('open');
});
//Dropdownlist-select
$('.drop-down ul li').click(function(){
    //Get Values of clicked list item
    let val = $( this ).attr('value');
    let content = $( this ).html();

    //Apply Values to titel and input box
    let dropdown = $( this ).closest('.drop-down');
    dropdown.find('[data-toggle]').html(content);
    dropdown.find('input[name="input-for-service"]').attr('value', val);

    //Close Select box
    $( this ).closest('.drop-down').toggleClass('open');

    //Set selected-property for styling
    $( this ).siblings('li').removeAttr('selected');
    $( this ).attr('selected', true);
});

//Dropdownlist-toggle
$('.drop-down .d-d-t').click(function(){
    $( this ).closest('.drop-down').toggleClass('open');
});
//Dropdownlist-select
$('.drop-down ul li').click(function(){
    //Get Values of clicked list item
    let val = $( this ).attr('value');
    let content = $( this ).html();

    //Apply Values to titel and input box
    let dropdown = $( this ).closest('.drop-down');
    dropdown.find('[data-toggle]').html(content);
    dropdown.find('input[name="input-for-type"]').attr('value', val);

    //Close Select box
    $( this ).closest('.drop-down').toggleClass('open');

    //Set selected-property for styling
    $( this ).siblings('li').removeAttr('selected');
    $( this ).attr('selected', true);
});

//Btn close-open
$('.op-cl-link').click(function (){
    $('.service-form').slideToggle();
});
$('.serv-btn-close').click(function (){
    $('.service-form').slideToggle();
});